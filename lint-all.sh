for f in `ls`; do
    if [ -d $f ] && [ -f "$f/package.json" ]; then
        cd $f
        echo
        echo "Now displaying linter issues for $f"

        npm run lint
        cd ..
    fi
done