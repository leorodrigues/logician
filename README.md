
This project is an experimental library of functions implementing the necessary operations to execute automated logical theorem solving. It is inspired in the book [Artificial Intelligence: Structures and Strategies for Complex Problem Solving](http://www.mypearsonstore.com/bookstore/artificial-intelligence-structures-and-strategies-for-9780321545893).

At the moment, the code is requiring a lot of revision and the test scenarios may be improved covering new situations. Also, the implementations may need performance optimizations.

The documentation on certain modules is rather lacking, i.e. not all functions are documented and only the main function - the one exported by the module - is thoroughly documented.

## Directory Structure

The project is comprised of many different packages under one single repository.
Some of them compose the core of the project and others are support or sattelite
packages:

- Core packages:
  - logician-api: <root>/api

- Client packages:
  - logician-experiments: <root>/experiments

- Support packages:
  - gulp-json: <root>/gulp-json
  - gulp-logician: <root>/gulp-logician

Further documentation is found inside each package.