for f in `ls`; do
    if [ -d "$f" ] && [ -f "$f/package.json" ]; then
        echo "Purging packages under $f"
        cd $f
        rm -rf node_modules
        rm -f package-lock.json
        cd ..
    fi
done
npm cache clean --force