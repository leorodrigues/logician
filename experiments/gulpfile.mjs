import path from 'path';
import gulp from 'gulp';
import gClean from 'gulp-clean';
import makeGulpLogician from '@leorodrigues/gulp-logician';

const DEFAULT_OPTIONS = { prettyPrint: true };

const gLogician = makeGulpLogician(DEFAULT_OPTIONS);

function makeParseExperimentTask(gulp, sources) {
    return gulp.parallel(function() {
        return gulp.src(sources)
            .pipe(gLogician.parseExperiment())
            .pipe(gLogician.collectExperiment());
    });
}

function makeLoadDatabaseTask(gulp) {
    return function() {
        return gulp.src('logician.db.json', { allowEmpty: true })
            .pipe(gLogician.loadDatabase());
    };
}

function makeSaveDatabaseTask(gulp) {
    return function() {
        return gLogician.saveDatabase({ path: 'logician.db.json' })
            .pipe(gulp.dest('./'));
    };
}

function makeCleanExperimentsTask(gulp, buildPath) {
    return function() {
        return gulp.src(`${buildPath}/*`, {read: false})
            .pipe(gClean({force: true}));
    };
}

function makeComputeCNFTask() {
    return () => gLogician.queryAllFormulas()
        .pipe(gLogician.computeCNF())
        .pipe(gLogician.simplifyCNF())
        .pipe(gLogician.patchFormula());
}

function makeIdentifyPredicateInstancesTask() {
    return () => gLogician.queryAllFormulas()
        .pipe(gLogician.identifyPredicates())
        .pipe(gLogician.patchFormula());
}

function makeShapeFormulasTask() {
    return () => gLogician.queryAllFormulas()
        .pipe(gLogician.shapeFormula())
        .pipe(gLogician.patchFormula());
}

function declareTasks(gulp, buildPath, sources) {
    buildPath = path.join(buildPath, 'experiments');

    gulp.task('load-database',
        makeLoadDatabaseTask(gulp));

    gulp.task('parse-experiment',
        makeParseExperimentTask(gulp, sources));

    gulp.task('save-database',
        makeSaveDatabaseTask(gulp));

    gulp.task('clean-experiments',
        makeCleanExperimentsTask(gulp, buildPath, sources));

    gulp.task('compute-cnf',
        makeComputeCNFTask(gulp));

    gulp.task('identify-predicates',
        makeIdentifyPredicateInstancesTask());

    gulp.task('shape-formulas',
        makeShapeFormulasTask());

    gulp.task('index-formulas',
        gulp.parallel([
            'identify-predicates',
            'shape-formulas'
        ]));

    gulp.task('import-experiments',
        gulp.series([
            'load-database',
            'parse-experiment',
            'save-database'
        ]));

    gulp.task('normalize',
        gulp.series([
            'load-database',
            'compute-cnf',
            'save-database'
        ]));

    gulp.task('index',
        gulp.series([
            'load-database',
            'index-formulas',
            'save-database'
        ]));

    gulp.task('experiments',
        gulp.series([
            'import-experiments',
            'normalize',
            'index'
        ]));

    return ['experiments', 'clean-experiments'];
}

declareTasks(gulp, './build', './src/*.xpr');
