[statement
    [key id choose-new]
    [fields
        [id [digest sha1 [realms] [premisses] [facts] [hypotheses]]]
        [realms [type string array]]
        [premisses [refer formula-set detached] [options optional]]
        [facts [refer formula-set detached] [options optional]]
        [hypotheses [refer formula-set detached] [options optional]]]]

[formula-set
    [key id keep-old]
    [fields
        [id [digest sha1 [realms] [formulas]]]
        [realms [inherit statement]]
        [formulas [refer formula array detached] [options whole-content]]]]

[formula
    [key id keep-old]
    [fields
        [id [digest sha1 [original]]]
        [realms [inherit statement]]
        [original [type raw] [options whole-content]]]]
