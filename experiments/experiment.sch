[experiment
    [key id fail]
    [fields
        [id [digest sha1 [realms] [theorem id] [facts id] [hipothesys id]]]
        [name [type string]]
        [realms [type string array]]
        [theorem [refer formula-list detached] [options optional]]
        [facts [refer formula-list detached] [options optional]]
        [hipothesys [refer formula detached] [options optional]]]]

[formula-list
    [key id keep-old]
    [fields
        [id [digest sha1 [realms] [formulas]]]
        [realms [inherit data]]
        [formulas [refer formula array detached] [options whole-content]]]]

[formula
    [key id keep-old]
    [fields
        [id [digest sha1 [original]]]
        [realms [inherit data]]
        [original [type raw] [options whole-content]]]]
