
import { parse, makeObjectAssemblingAndMergingFns }
    from '@leorodrigues/list-struts';

import fs from 'fs';

const schemaSource = fs.readFileSync('statement.sch', 'utf-8');

const schema = parse(schemaSource);
const [ assemble, merge ] =
    makeObjectAssemblingAndMergingFns(schema);

const samplesSource = fs.readFileSync('src/dev-poc-3.stmt', 'utf-8');
const samples = parse(samplesSource);

const sampleStatements = samples.map(s => assemble.statement(s));
const instances = merge.statement(sampleStatements);

console.log(JSON.stringify(instances, null, 4));
