
SYSTEM=$(uname)

if [[ $SYSTEM == "Darwin" ]]; then
    EXEC=node
else
    EXEC=node.exe
fi

$EXEC node-developer-tools/index.mjs \
    project dependencies sync plan \
    -s @leorodrigues/developer-tools \
    -s @leorodrigues/logician-experiments \
    --no-push-commits \
    --no-push-tags
