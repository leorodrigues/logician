for f in `ls`; do
    if [ -d "$f" ] && [ -f "$f/package.json" ]; then
        echo "Installing packages under $f"
        cd $f
        npm i
        cd ..
    fi
done